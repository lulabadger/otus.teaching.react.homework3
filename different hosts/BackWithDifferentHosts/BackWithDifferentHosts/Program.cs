var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

string[]? corsOrigins = builder.Configuration.GetSection("CORS:Origins").Get<string[]>();
corsOrigins ??= [];
string[]? corsHeaders = builder.Configuration.GetSection("CORS:Headers").Get<string[]>();
corsHeaders ??= [];
string[]? corsMethods = builder.Configuration.GetSection("CORS:Methods").Get<string[]>();
corsMethods ??= [];

builder.Services.AddCors(options =>
{
    options.AddPolicy(name: MyAllowSpecificOrigins,
                      policy =>
                      {
                          policy.WithOrigins(corsOrigins)
                          .WithHeaders(corsHeaders)
                          .WithMethods(corsMethods);
                      });
});

builder.Services.AddControllers();

//Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();

app.UseCors(MyAllowSpecificOrigins);

app.UseAuthorization();

app.MapControllers();

app.Run();
