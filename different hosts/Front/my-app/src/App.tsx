import React from 'react';
import { useEffect, useState } from 'react'
import './App.css';


export type WeatherForecast = {
  id: string;
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}


function App() {
  const [error, setError] = useState(false);
  const [forecast, setForecast] = useState<WeatherForecast[]>([]);
  useEffect(() => {
    fetchData(`${process.env.REACT_APP_BACK_URL}/WeatherForecast`);
  }, [])

  async function fetchData(url: string) {
    try {
      const response = await fetch(url)
      if (response.ok) {
        const data = await response.json();
        setForecast(data);
      } else {
        setError(true);
      }
    } catch (error) {
      setError(true);
    }
  }

  return (
    <div className="App-header">
      <h1>WeatherForecast</h1>
      {error === false && forecast.map(p => (
        <div key={p.id}>
          <h3>{p.date}</h3>
          <p>Температура: {p.temperatureC} °C</p>  
          <p>{p.summary}</p>
        </div>
      ))}

      {error === true && (
        <>
          <p>Ошибка</p>
        </>
      )}

    </div>
  );
}

export default App;
